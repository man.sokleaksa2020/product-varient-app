@if (count($combinations[0]) > 0)
    <table
        class="border-collapse border dark:border-gray-600 mt-0 w-full whitespace-nowrap [&>tbody>*:nth-child(odd)]:bg-slate-100 [&>tbody>*>*]:py-1.5">
        <thead class="!bg-slate-200 font-medium dark:bg-gray-800">
            <tr>
                <th class="border-b cursor-pointer">
                    <label for="" class="control-label">Variant</label>
                </th>
                <th class="border-b cursor-pointer">
                    <label for="" class="control-label">Variant Price</label>
                </th>
                <th class="border-b cursor-pointer">
                    <label for="" class="control-label">SKU</label>
                </th>
                <th class="border-b cursor-pointer">
                    <label for="" class="control-label">Quantity</label>
                </th>
            </tr>
        </thead>
        <tbody>
@endif
@foreach ($combinations as $key => $combination)
    @php
        $sku = '';
        foreach (explode(' ', $product_name) as $key => $value) {
            $sku .= substr($value, 0, 1);
        }

        $str = '';
        foreach ($combination as $key => $item) {
            if ($key > 0) {
                $str .= '-' . str_replace(' ', '', $item);
                $sku .= '-' . str_replace(' ', '', $item);
            } else {
                if ($colors_active == 1) {
                    $color_name = \App\Model\Color::where('code', $item)->first()->name;
                    $str .= $color_name;
                    $sku .= '-' . $color_name;
                } else {
                    $str .= str_replace(' ', '', $item);
                    $sku .= '-' . str_replace(' ', '', $item);
                }
            }
        }
    @endphp

    @if (strlen($str) > 0)
        <tr>
            <td class="text-center">
                <label for="" class="control-label text-center">{{ $str }}</label>
            </td>
            <td class="text-center">
                <input type="number" name="price_{{ $str }}" value="{{ $unit_price }}" min="0"
                    step="0.01" class="form-control text-center" required>
            </td>
            <td class="text-center">
                <input type="text" name="sku_{{ $str }}" value="{{ $sku }}" class="form-control text-center"
                    required>
            </td>
            <td class="text-center">
                <input type="number" name="qty_{{ $str }}" value="1" min="1" max="1000000"
                    step="1" class="form-control text-center" required>
            </td>
        </tr>
    @endif
@endforeach
</tbody>
</table>

<script>
    update_qty();

    function update_qty() {
        var total_qty = 0;
        var qty_elements = $('input[name^="qty_"]');
        for (var i = 0; i < qty_elements.length; i++) {
            total_qty += parseInt(qty_elements.eq(i).val());
        }
        if (qty_elements.length > 0) {
            $('input[name="current_stock"]').attr("readonly", true);
            $('input[name="current_stock"]').val(total_qty);
        } else {
            $('input[name="current_stock"]').attr("readonly", false);
        }
    }
    $('input[name^="qty_"]').on('keyup', function() {
        var total_qty = 0;
        var qty_elements = $('input[name^="qty_"]');
        for (var i = 0; i < qty_elements.length; i++) {
            total_qty += parseInt(qty_elements.eq(i).val());
        }
        $('input[name="current_stock"]').val(total_qty);
    });
</script>
