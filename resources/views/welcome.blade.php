<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <title>Laravel</title>
</head>

<body>
    <form action="{{ route('product.store') }}" method="POST" enctype="multipart/form-data" id="product_form">

        @csrf
        <div class=" border m-5">
            {{-- new input --}}
            <div class="container relative w-full">
                <span class="">Attributes </span>
                <div class="select-btn border border-gray-300 rounded-md cursor-pointer">
                    <span class="btn-text flex gap-2 p-1 h-8">

                    </span>
                </div>

                <ul class="list-items absolute w-full bg-slate-100 rounded-md mt-2 hidden">
                    <li class="item border-b" id="1" onclick="add_more_customer_choice_option('1', 'HTML&CSS')">
                        <span class="checkbox">
                            <i class="fa-solid fa-check check-icon"></i>
                        </span>
                        <span class="item-text px-1" id="HTML&CSS">HTML & CSS</span>
                        <input id="input" type="hidden" value="1">
                    </li>
                    <li class="item border-b" id="2"
                        onclick="add_more_customer_choice_option('2', 'Bootstrap')">
                        <span class="checkbox">
                            <i class="fa-solid fa-check check-icon"></i>
                        </span>
                        <span class="item-text px-1">Bootstrap</span>
                        <input id="input" type="hidden" value="2">
                    </li>
                    <li class="item border-b" id="3"
                        onclick="add_more_customer_choice_option('3', 'JavaScript')">
                        <span class="checkbox">
                            <i class="fa-solid fa-check check-icon"></i>
                        </span>
                        <span class="item-text px-1">JavaScript</span>
                        <input id="input" type="hidden" value="3">
                    </li>
                </ul>
            </div>


            {{-- old input --}}
            <label class="text-base font-medium p-3" for="status">Variations <span
                    class="text-red-500">*</span></label>
            <div class="grid grid-cols-2 gap-3 items-center justify-between mt-2 px-3">
                <select name="choice_attributes[]" id="choice_attributes" multiple>
                    <option value="0">-- SELECT --</option>
                    <option value="1">Size</option>
                    <option value="3">Color</option>
                    <option value="2">Style</option>
                </select>
            </div>

            <div class="mt-4">
                <div class=" customer_choice_options" id="customer_choice_options"></div>
            </div>
            <div class="pt-4 col-12 sku_combination" id="sku_combination">

            </div>

        </div>

        <button type="button" onclick="submit()" class="btn btn-primary float-right">Submit</button>

    </form>
</body>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap4-tagsinput@4.1.3/tagsinput.min.css">
<script src="https://cdn.jsdelivr.net/npm/bootstrap4-tagsinput@4.1.3/tagsinput.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E="
    crossorigin="anonymous"></script>
<script>
    // product varient
    // $('#choice_attributes').on('change', function() {
    //     $('#customer_choice_options').html(null);
    //     $.each($("#choice_attributes option:selected"), function() {
    //         // add_more_customer_choice_option($(this).val(), $(this).text());
    //         // console.log(12,$(this).val());
    //     });
    // });

    // function add_more_customer_choice_option(i, name) {
    //     let n = name.split(' ').join('');
    //     $('#customer_choice_options').append(
    //         '<div class="grid grid-cols-2 gap-3 px-3 pb-3"><input type="hidden" name="choice_no[]" value="' + i +
    //         '"><input type="text" class="col-span-1 px-2 py-1 border rounded-lg hover:border-blue-500 focus:outline-none" name="choice[]" value="' +
    //         n +
    //         '" readonly><input type="text" class="col-span-1 px-2 py-1 border rounded-lg hover:border-blue-500 focus:outline-none focus:border-blue-500" name="choice_options_' +
    //         i + '[]" placeholder="Enter choice values" data-role="tagsinput" onchange="update_sku()"></div>');
    // }

    // input select 
    const selectBtn = document.querySelector(".select-btn"),
        listItems = document.querySelector(".list-items"),
        btnText = document.querySelector(".btn-text"),
        items = document.querySelectorAll(".item");

    selectBtn.addEventListener("click", () => {
        listItems.classList.toggle("hidden");
    });
    var index = 1;
    items.forEach(item => {
        item.addEventListener("click", () => {
            item.classList.toggle("checked");
            let checked = document.querySelectorAll(".checked"),
                btnText = document.querySelector(".btn-text");
            if (checked && checked.length > 0) {
                let liTag =
                    `<div class="flex items-center justify-center gap-1 p-1 border border-gray-300 rounded-md">${item.innerText}<div class="uit uit-multiply flex items-center justify-center flex-shrink-0 bg-gray-200 backdrop-blur-sm w-5 h-5 text-xs rounded-full" onclick="remove(this,1) "</div></div>`;
                $(".btn-text").append(liTag);
                item.classList.add("hidden");
                listItems.classList.remove("hidden");
            } else {
                btnText.innerText = "Select Language";
            }
        });
    })

    function add_more_customer_choice_option(i, name) {
        let n = name.split(' ').join(''),
            x = 1;
        $('#customer_choice_options').append(
            '<div id="appendIndex'+ i +'" class="grid grid-cols-2 gap-3 px-3 pb-3"><input type="hidden" name="choice_no[]" value="' + i +
            '"><input type="text" class="col-span-1 px-2 py-1 border rounded-lg hover:border-blue-500 focus:outline-none" name="choice[]" value="' +
            n +
            '" readonly><input type="text" class="col-span-1 px-2 py-1 border rounded-lg hover:border-blue-500 focus:outline-none focus:border-blue-500" name="choice_options_' +
            i + '[]" placeholder="Enter choice values" data-role="tagsinput" onchange="update_sku()"></div>');
    }

    function remove(element, i) {
        items.forEach(item => {
            let listItems = document.querySelector(".list-items"),
                checked = document.querySelectorAll(".checked");
            item.classList.remove("checked");
            element.parentElement.remove();

            item.classList.remove("hidden");
            listItems.classList.remove("hidden");
            // let removeAppend = document.getElementById("appendIndex");
            const appendIndex = $("#appendIndex" + i);
            appendIndex.remove();
        });
    }

    function update_sku() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: '{{ route('variant') }}',
            data: $('#product_form').serialize(),
            success: function(data) {
                $('#sku_combination').html(data.view);
                if (data.length > 1) {
                    $('#quantity').hide();
                } else {
                    $('#quantity').show();
                }
            }
        });
    }

    function submit() {
        var formData = new FormData(document.getElementById('product_form'));
        console.log(formData);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.post({
            url: '{{ route('product.store') }}',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data) {
                if (data.errors) {
                    for (var i = 0; i < data.errors.length; i++) {
                        toastr.error(data.errors[i].message, {
                            CloseButton: true,
                            ProgressBar: true
                        });
                    }
                } else {
                    $('#product_form').submit();
                }
            }
        });
    }
</script>


</html>
